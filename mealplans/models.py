from django.conf import settings
from django.db import models
# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL


class MealPlan(models.Model):
    name = models.CharField(max_length=125)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="mealplans",
        on_delete=models.CASCADE
    )
    recipes = models.ManyToManyField(
        "recipes.Recipe",
        related_name="mealplans",
        )
