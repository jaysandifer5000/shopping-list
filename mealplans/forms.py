from django.forms import ModelForm

from mealplans.models import MealPlan


class MealPlanForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = [
            "name",
            "recipes"
         ]


class MealPlanDeleteForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = []