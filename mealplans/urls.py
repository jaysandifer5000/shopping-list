from django.urls import path

from mealplans.views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanUpdateView,
    MealPlanDetailView,
    MealPlanListView,
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplan_list"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="mealplan_detail"),
    path(
        "<int:pk>/delete/", MealPlanDeleteView.as_view(), name="mealplan_delete"
    ),
    path("create/", MealPlanCreateView.as_view(), name="mealplan_new"),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="mealplan_edit"),
]
